from django.db import models
from django.shortcuts import reverse


class Company(models.Model):
    c_full_name = models.CharField(max_length=255,
                                   db_index=True,
                                   verbose_name='Полное название')
    c_short_name = models.CharField(max_length=32,
                                    db_index=True,
                                    verbose_name='Краткое название')

    def __str__(self):
        return self.c_short_name

    class Meta:
        ordering = ['c_short_name']
        unique_together = ('c_short_name', 'c_full_name')


class EducationEstablishment(models.Model):
    e_full_name = models.CharField(max_length=255,
                                   db_index=True,
                                   verbose_name='Полное название')
    e_short_name = models.CharField(max_length=32,
                                    db_index=True,
                                    verbose_name='Краткое название')

    def __str__(self):
        return f'{self.e_short_name} | {self.e_full_name}'

    class Meta:
        ordering = ['e_short_name']
        unique_together = ('e_short_name', 'e_full_name')


class OrganizationStructure(models.Model):
    organizational_unit = models.CharField(
        max_length=255,
        db_index=True,
        verbose_name='Структурное подразделение'
    )
    parent_ou_id = models.ForeignKey(to='OrganizationStructure',
                                     on_delete=models.CASCADE,
                                     null=True, blank=True,
                                     verbose_name='Вышестоящее подразделение')
    company_id = models.ForeignKey(Company,
                                   on_delete=models.CASCADE,
                                   verbose_name='Компания')

    def __str__(self):
        return self.organizational_unit


class Person(models.Model):
    WOMAN = 0
    MAN = 1
    SEX = (
        (WOMAN, 'женский'),
        (MAN, 'мужской')
    )

    last_name = models.CharField(max_length=64, db_index=True,
                                 verbose_name='Фамилия')
    first_name = models.CharField(max_length=64, db_index=True,
                                  verbose_name='Имя')
    middle_name = models.CharField(max_length=64, blank=True,
                                   verbose_name='Отчество')
    sex = models.IntegerField(choices=SEX, verbose_name='Пол')
    birth_date = models.DateField(verbose_name='Дата рождения')
    address = models.CharField(max_length=255,
                               blank=True,
                               verbose_name='Адрес')

    def __str__(self):
        return '{} {}'.format(self.last_name, self.first_name)

    def get_full_name(self):
        return '{} {} {}'.format(self.last_name, self.first_name,
                                 self.middle_name)

    def get_absolute_url(self):
        return reverse('hrdb:officeseeker_detail', kwargs={'pk': self.id})


class OfficeSeeker(Person, models.Model):
    NOT_MEETING = 0
    GOOD_FOR = 1
    EXCLUDE = 2
    RESERVE = 3
    EMPLOYED = 4
    STATUS = (
        (NOT_MEETING, 'не собеседовался'),
        (GOOD_FOR, 'подходит'),
        (EXCLUDE, 'исключить'),
        (RESERVE, 'резерв'),
        (EMPLOYED, 'принят на работу'),
    )
    INSIDE = 0
    WORK_UA = 1
    HH = 2
    _0629 = 3
    OLX = 4
    RABOTA_UA = 5
    SOURCE = (
        (INSIDE, 'внутренний'),
        (WORK_UA, 'work.ua'),
        (HH, 'headHunter'),
        (_0629, '0629'),
        (OLX, 'olx'),
        (RABOTA_UA, 'rabota.ua'),
    )

    status = models.IntegerField(choices=STATUS, default=NOT_MEETING,
                                 verbose_name='Статус')
    source = models.IntegerField(choices=SOURCE, default=INSIDE,
                                 verbose_name='Источник')


class PersonsEmail(models.Model):
    email = models.EmailField(verbose_name='eMail', blank=False)
    person_id = models.ForeignKey(Person, on_delete=models.CASCADE,
                                  verbose_name='Ф.И.О.',
                                  related_name='personemails')

    def __str__(self):
        return '{} - {}'.format(self.email, self.person_id)


class PersonPhone(models.Model):
    phone = models.CharField(max_length=20,
                             blank=False,
                             verbose_name='Телефон')
    person = models.ForeignKey(Person, on_delete=models.CASCADE,
                               verbose_name='Ф.И.О.',
                               related_name='personphones')

    def __str__(self):
        return '{}: {}'.format(self.phone, self.person)


class Educational(models.Model):
    QUALIFICATION = [
        (1, 'среднее'),
        (2, 'среднее специальное'),
        (3, 'неоконченное высшее'),
        (4, 'высшее'),
    ]
    person = models.ForeignKey(Person,
                               on_delete=models.CASCADE,
                               verbose_name='Ф.И.О.',
                               related_name='person_edicational')
    education_establishment = models.ForeignKey(
            EducationEstablishment,
            on_delete=models.CASCADE,
            verbose_name='Учебное заведение',
            related_name='person_edu')
    year_graduated = models.PositiveSmallIntegerField(
            verbose_name='Год окончания', blank=True, null=True)
    profession = models.CharField(max_length=255, verbose_name='Специальность')
    qualification = models.PositiveSmallIntegerField(
            choices=QUALIFICATION,
            default=1,
            verbose_name='Квалификация')
