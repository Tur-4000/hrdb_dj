from django.urls import path

from . import views

app_name = 'hrdb'
urlpatterns = [
    path('', views.HomePageView.as_view(), name='index'),
    path('companies/', views.CompanyListView.as_view(), name='companies'),
    path('company/new/',
         views.CompanyCreateView.as_view(),
         name='company_new'),
    path('company/<int:pk>/edit/',
         views.CompanyUpdateView.as_view(),
         name='company_edit'),
    path('company/<int:pk>/delete/',
         views.CompanyDeleteView.as_view(),
         name='company_delete'),
    path('educationestablishments/',
         views.EducationEstablishmentListView.as_view(),
         name='educationestablishments'),
    path('educationestablishment/new/',
         views.EducationEstablishmentCreateView.as_view(),
         name='educationestablishment_new'),
    path('educationestablishment/<int:pk>/edit/',
         views.EducationEstablishmentUpdateView.as_view(),
         name='educationestablishment_edit'),
    path('educationestablishment/<int:pk>/delete/',
         views.EducationEstablishmentDeleteView.as_view(),
         name='educationestablishment_delete'),
    path('orgstructure/',
         views.OrgStructureListView.as_view(),
         name='orgstructure'),
    path('orgstructure/new/',
         views.OrgStructureCreateView.as_view(),
         name='orgstructure_new'),
    path('orgstructure/<int:pk>/edit/',
         views.OrgStructureUpdateView.as_view(),
         name='orgstructure_edit'),
    path('officeseekers/',
         views.OfficeSeekerListView.as_view(),
         name='officeseekers'),
    path('officeseekers/<int:pk>',
         views.OfficeSeekerDetailView.as_view(),
         name='officeseeker_detail'),
    path('officeseekers/new/',
         views.OfficeSeekerCreateView.as_view(),
         name='officeseeker_new'),
    path('officeseekers/<int:pk>/edit/',
         views.OfficeSeekerUpdateView.as_view(),
         name='officeseeker_edit'),
    path('personemail/<int:person_id>/add/',
         views.person_email_create,
         name='personemail_add'),
    path('personemail/<int:person_id>/edit/<int:email_id>',
         views.person_email_edit,
         name='personemail_edit'),
    path('personphone/<int:person_id>/add/',
         views.person_phone_create,
         name='personphone_add'),
    path('personphone/<int:person_id>/edit/<int:phone_id>',
         views.person_phone_edit,
         name='personphone_edit'),
    path('personeducational/<int:person_id>/add/',
         views.person_educational_create,
         name='personeducational_add')
]
