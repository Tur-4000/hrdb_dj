from django.contrib import admin

from .models import Company, EducationEstablishment, OrganizationStructure, \
    Person, OfficeSeeker, PersonsEmail, PersonPhone


class EmailInline(admin.StackedInline):
    model = PersonsEmail
    extra = 1


class PhoneInline(admin.StackedInline):
    model = PersonPhone
    extra = 1


class PersonsEmailAdmin(admin.ModelAdmin):
    list_display = (
        'email', 'person_id'
    )
    search_fields = (
        'email', 'person_id'
    )
    autocomplete_fields = ('person_id',)


class OfficeSeekerAdmin(admin.ModelAdmin):
    list_display = (
        'last_name', 'first_name', 'middle_name', 'status')
    list_filter = (
        'status',
        'source',
    )
    inlines = [
        EmailInline,
        PhoneInline,
    ]
    fields = (
        'last_name',
        'first_name',
        'middle_name',
        'sex',
        'birth_date',
        'address',
        'status',
        'source',
    )
    search_fields = (
        'last_name', 'first_name',)

    def name(self, obj):
        return "{} {}".format(obj.first_name, obj.last_name)


class PersonAdmin(admin.ModelAdmin):
    list_display = (
        'last_name', 'first_name', 'middle_name')
    search_fields = (
        'last_name', 'first_name',)
    inlines = [
        EmailInline,
        PhoneInline,
    ]

    def name(self, obj):
        return "{} {}".format(obj.first_name, obj.last_name)


class CompanyAdmin(admin.ModelAdmin):
    list_display = (
        'c_short_name', 'c_full_name'
    )


class EducationEstablishmentAdmin(admin.ModelAdmin):
    list_display = (
        'e_short_name', 'e_full_name'
    )


admin.site.register(Company, CompanyAdmin)
admin.site.register(EducationEstablishment, EducationEstablishmentAdmin)
admin.site.register(OrganizationStructure)
admin.site.register(Person, PersonAdmin)
admin.site.register(OfficeSeeker, OfficeSeekerAdmin)
admin.site.register(PersonsEmail, PersonsEmailAdmin)
admin.site.register(PersonPhone)
