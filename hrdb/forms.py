from django import forms

from .models import Company, EducationEstablishment, OrganizationStructure, \
    Person, OfficeSeeker, PersonsEmail, PersonPhone, Educational


class CompanyForm(forms.ModelForm):
    class Meta:
        model = Company
        fields = ['c_full_name', 'c_short_name']
        widgets = {
            'c_full_name': forms.TextInput(attrs={'class': 'form-control'}),
            'c_short_name': forms.TextInput(attrs={'class': 'form-control'}),
        }


class EducationEstablishmentForm(forms.ModelForm):
    class Meta:
        model = EducationEstablishment
        fields = ['e_full_name', 'e_short_name']
        widgets = {
            'e_full_name': forms.TextInput(attrs={'class': 'form-control'}),
            'e_short_name': forms.TextInput(attrs={'class': 'form-control'}),
        }


class OrgStructureForm(forms.ModelForm):
    class Meta:
        model = OrganizationStructure
        fields = ['company_id', 'parent_ou_id', 'organizational_unit']
        widgets = {
            'company_id': forms.Select(attrs={'class': 'form-control'}),
            'parent_ou_id': forms.Select(attrs={'class': 'form-control'}),
            'organizational_unit': forms.TextInput(attrs={'class': 'form-control'}),
        }


class OfficeSeekerForm(forms.ModelForm):
    # email = forms.EmailField(
    #     widget=forms.EmailInput(attrs={'class': 'form-control'}),
    # )

    class Meta:
        model = OfficeSeeker
        fields = ['last_name', 'first_name', 'middle_name', 'birth_date',
                  'sex', 'address', 'source', 'status']
        widgets = {
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'middle_name': forms.TextInput(attrs={'class': 'form-control'}),
            'sex': forms.Select(attrs={'class': 'form-control'}),
            'birth_date': forms.DateInput(attrs={'class': 'form-control',
                                                 'type': 'date'}),
            'address': forms.Textarea(attrs={'cols': '40', 'rows': '2',
                                             'class': 'form-control'}),
            # 'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'source': forms.Select(attrs={'class': 'form-control'}),
            'status': forms.Select(attrs={'class': 'form-control'}),
        }


class PersonEmailForm(forms.ModelForm):

    class Meta:
        model = PersonsEmail
        fields = ['email', 'person_id']
        widgets = {
            'email': forms.EmailInput(attrs={'class': 'form-control',
                                             'placeholder': 'eMail'}),
            'person_id': forms.HiddenInput(),
        }


class PersonPhoneForm(forms.ModelForm):

    class Meta:
        model = PersonPhone
        fields = ['phone', 'person']
        widgets = {
            'phone': forms.TextInput(attrs={'class': 'form-control',
                                            'placeholder': 'Номер телефона'}),
            'person': forms.HiddenInput(),
        }


class PersonEducationalForm(forms.ModelForm):

    class Meta:
        model = Educational
        fields = ['person', 'education_establishment', 'year_graduated',
                  'profession', 'qualification']
        widgets = {
            'person': forms.HiddenInput(),
            'education_establishment': forms.Select(),
            'year_graduated': forms.NumberInput(),
            'profession': forms.TextInput(),
            'qualification': forms.Select(),
        }
