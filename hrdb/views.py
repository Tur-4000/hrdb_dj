from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin

from .forms import CompanyForm, EducationEstablishmentForm, OrgStructureForm, \
    OfficeSeekerForm, PersonEmailForm, PersonPhoneForm, PersonEducationalForm
from .models import Company, EducationEstablishment, OrganizationStructure, \
    Person, OfficeSeeker, PersonsEmail, PersonPhone, Educational


class HomePageView(TemplateView):
    template_name = 'hrdb/index.html'


# Компании группы "МИГ"
class CompanyListView(LoginRequiredMixin, ListView):
    model = Company
    template_name = 'hrdb/company_list.html'


class CompanyCreateView(LoginRequiredMixin, CreateView):
    form_class = CompanyForm
    template_name = 'hrdb/company_create.html'
    success_url = reverse_lazy('hrdb:companies')


class CompanyUpdateView(LoginRequiredMixin, UpdateView):
    model = Company
    form_class = CompanyForm
    template_name = 'hrdb/company_edit.html'
    success_url = reverse_lazy('hrdb:companies')


class CompanyDeleteView(LoginRequiredMixin, DeleteView):
    model = Company
    template_name = 'hrdb/company_delete.html'
    success_url = reverse_lazy('hrdb:companies')


# Учебные заведения
class EducationEstablishmentListView(LoginRequiredMixin, ListView):
    model = EducationEstablishment
    template_name = 'hrdb/EducationEstablishment_list.html'


class EducationEstablishmentCreateView(LoginRequiredMixin, CreateView):
    form_class = EducationEstablishmentForm
    template_name = 'hrdb/EducationEstablishment_create.html'
    success_url = reverse_lazy('hrdb:educationestablishments')


class EducationEstablishmentUpdateView(LoginRequiredMixin, UpdateView):
    model = EducationEstablishment
    form_class = EducationEstablishmentForm
    template_name = 'hrdb/EducationEstablishment_edit.html'
    success_url = reverse_lazy('hrdb:educationestablishments')


class EducationEstablishmentDeleteView(LoginRequiredMixin, DeleteView):
    model = EducationEstablishment
    template_name = 'hrdb/EducationEstablishment_delete.html'
    success_url = reverse_lazy('hrdb:educationestablishments')


# Оргструктура
class OrgStructureListView(LoginRequiredMixin, ListView):
    model = OrganizationStructure
    template_name = 'hrdb/organizationstructure_list.html'


class OrgStructureCreateView(LoginRequiredMixin, CreateView):
    form_class = OrgStructureForm
    template_name = 'hrdb/organizationstructure_create.html'
    success_url = reverse_lazy('hrdb:orgstructure')


class OrgStructureUpdateView(LoginRequiredMixin, UpdateView):
    model = OrganizationStructure
    form_class = OrgStructureForm
    template_name = 'hrdb/organizationstructure_update.html'
    success_url = reverse_lazy('hrdb:orgstructure')


# Соискатели/Персонал
class OfficeSeekerListView(LoginRequiredMixin, ListView):
    model = OfficeSeeker
    template_name = 'hrdb/officeseeker_list.html'


class OfficeSeekerDetailView(LoginRequiredMixin, DetailView):
    model = OfficeSeeker
    template_name = 'hrdb/officeseeker_detail.html'


class OfficeSeekerCreateView(LoginRequiredMixin, CreateView):
    form_class = OfficeSeekerForm
    template_name = 'hrdb/officeseeker_new.html'
    success_url = reverse_lazy('hrdb:officeseekers')


class OfficeSeekerUpdateView(LoginRequiredMixin, UpdateView):
    model = OfficeSeeker
    form_class = OfficeSeekerForm
    template_name = 'hrdb/officeseeker_new.html'
    success_url = reverse_lazy('hrdb:officeseekers')


def person_email_create(request, person_id):
    if request.method == 'POST':
        PersonsEmail.objects.create(
            email=request.POST['email'],
            person_id=Person.objects.get(id=request.POST['person_id'])
        )
        return redirect('hrdb:officeseeker_detail', pk=person_id)
    else:
        person = Person.objects.get(id=person_id)
        form = PersonEmailForm(initial={'person_id': person_id})
        return render(request, 'hrdb/person_email.html', {'form': form,
                                                          'person': person})


def person_email_edit(request, person_id, email_id):
    if request.method == 'POST':
        person_email = PersonsEmail.objects.get(id=email_id)
        person_email.email = request.POST['email']
        person_email.save()
        return redirect('hrdb:officeseeker_detail', pk=person_id)
    else:
        person = Person.objects.get(id=person_id)
        email = PersonsEmail.objects.get(id=email_id)
        form = PersonEmailForm(initial={'person_id': person_id,
                                        'email': email.email})
        context = {'form': form, 'person': person, 'email': email.email}
        return render(request, 'hrdb/person_email.html', context)


def person_phone_create(request, person_id):
    if request.method == 'POST':
        PersonPhone.objects.create(
            phone=request.POST['phone'],
            person=Person.objects.get(id=request.POST['person'])
        )
        return redirect('hrdb:officeseeker_detail', pk=person_id)
    else:
        person = Person.objects.get(id=person_id)
        form = PersonPhoneForm(initial={'person': person_id})
        return render(request, 'hrdb/person_phone.html', {'form': form,
                                                          'person': person})


def person_phone_edit(request, person_id, phone_id):
    if request.method == 'POST':
        person_phone = PersonPhone.objects.get(id=phone_id)
        person_phone.phone = request.POST['phone']
        person_phone.save()
        return redirect('hrdb:officeseeker_detail', pk=person_id)
    else:
        person = Person.objects.get(id=person_id)
        phone = PersonPhone.objects.get(id=phone_id)
        form = PersonPhoneForm(initial={'person_id': person.id,
                                        'phone': phone.id})
        context = {'form': form, 'person': person, 'phone': phone.phone}
        return render(request, 'hrdb/person_phone.html', context)


def person_educational_create(request, person_id):
    if request.method == 'POST':
        Educational.objects.create(
            person=Person.objects.get(id=request.POST['person']),
            education_establishment=EducationEstablishment.objects.get(
                id=request.POST['education_establishment']),
            profession=request.POST['profession'],
            qualification=request.POST['qualification']
        )
        return redirect('hrdb:officeseeker_detail', pk=person_id)
    else:
        person = Person.objects.get(id=person_id)
        form = PersonEducationalForm(initial={'person': person.id})
        return render(request, 'hrdb/person_educational.html',
                      {'form': form, 'person': person})
