# Generated by Django 2.1.2 on 2018-12-06 09:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('c_full_name', models.CharField(db_index=True, max_length=255, verbose_name='Полное название')),
                ('c_short_name', models.CharField(db_index=True, max_length=32, verbose_name='Краткое название')),
            ],
            options={
                'ordering': ['c_short_name'],
            },
        ),
        migrations.CreateModel(
            name='EducationEstablishment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('e_full_name', models.CharField(db_index=True, max_length=255, verbose_name='Полное название')),
                ('e_short_name', models.CharField(db_index=True, max_length=32, verbose_name='Краткое название')),
            ],
            options={
                'ordering': ['e_short_name'],
            },
        ),
        migrations.CreateModel(
            name='OrganizationStructure',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('organizational_unit', models.CharField(db_index=True, max_length=255, verbose_name='Структурное подразделение')),
                ('company_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='hrdb.Company', verbose_name='Компания')),
                ('parent_ou_id', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='hrdb.OrganizationStructure', verbose_name='Вышестоящее подразделение')),
            ],
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('last_name', models.CharField(db_index=True, max_length=64, verbose_name='Фамилия')),
                ('first_name', models.CharField(db_index=True, max_length=64, verbose_name='Имя')),
                ('middle_name', models.CharField(blank=True, max_length=64, verbose_name='Отчество')),
                ('sex', models.IntegerField(choices=[(0, 'женский'), (1, 'мужской')], verbose_name='Пол')),
                ('birth_date', models.DateField(verbose_name='Дата рождения')),
                ('address', models.CharField(blank=True, max_length=255, verbose_name='Адрес')),
            ],
        ),
        migrations.CreateModel(
            name='PersonsEmail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=254, verbose_name='eMail')),
            ],
        ),
        migrations.CreateModel(
            name='OfficeSeeker',
            fields=[
                ('person_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='hrdb.Person')),
                ('status', models.IntegerField(choices=[(0, 'не собеседовался'), (1, 'подходит'), (2, 'исключить'), (3, 'резерв'), (4, 'принят на работу')], default=0, verbose_name='Статус')),
                ('source', models.IntegerField(choices=[(0, 'внутренний'), (1, 'work.ua'), (2, 'headHunter'), (3, '0629'), (4, 'olx'), (5, 'rabota.ua')], default=0, verbose_name='Источник')),
            ],
            bases=('hrdb.person', models.Model),
        ),
        migrations.AddField(
            model_name='personsemail',
            name='person_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='hrdb.Person', verbose_name='Ф.И.О.'),
        ),
        migrations.AlterUniqueTogether(
            name='educationestablishment',
            unique_together={('e_short_name', 'e_full_name')},
        ),
        migrations.AlterUniqueTogether(
            name='company',
            unique_together={('c_short_name', 'c_full_name')},
        ),
    ]
