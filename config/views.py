from django.shortcuts import redirect


def redirect_index(request):
    return redirect('hrdb:index', permanent=False)
